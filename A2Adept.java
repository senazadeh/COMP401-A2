package a2;

import java.util.Scanner;

public class A2Adept {

	public static void main(String[] args) {
		
		Scanner s = new Scanner(System.in); 
 		
 		int numberOfIngredients = s.nextInt();
 		
 		String[] listOfIngredientNames = new String[numberOfIngredients];
 		double[] listOfPricePerOunce = new double[numberOfIngredients];
 		boolean[] listOfIsVegetarian = new boolean[numberOfIngredients];
 		int[] listOfCaloriesPerOunce = new int[numberOfIngredients];
 		
 		for (int i = 0; i < numberOfIngredients; i++) {
 			String ingredientName = s.next();
 			listOfIngredientNames[i] = ingredientName;
 			double pricePerOunce = s.nextDouble();
 			listOfPricePerOunce[i] = pricePerOunce;
 			boolean isVegtarian = s.nextBoolean();
 			listOfIsVegetarian[i] = isVegtarian;
 			int caloriesPerOunce = s.nextInt();
 			listOfCaloriesPerOunce[i] = caloriesPerOunce;
 		}
 		
 		int numberOfRecipes = s.nextInt();
 		
 		String[] listOfRecipeNames = new String[numberOfRecipes];
 		double[] listOfRecipeCalories = new double[numberOfRecipes];
 		double[] listOfRecipePrices = new double[numberOfRecipes];
 		boolean[] listOfRecipeVegetarian = new boolean[numberOfRecipes];
 		for (int i = 0; i < listOfRecipeVegetarian.length; i++) {
 			listOfRecipeVegetarian[i] = true; }
 		
 		for (int i = 0; i < numberOfRecipes; i++) {
 			String recipeName = s.next();
 			listOfRecipeNames[i] = recipeName;
 			int numberOfIngredientsNeeded = s.nextInt();
 			for (int j = 0; j < numberOfIngredientsNeeded; j++) {
 				String recipeIngredient = s.next();
 				double ouncesOfIngredient = s.nextDouble();
 				for (int k = 0; k < listOfIngredientNames.length; k++) {
 					if (listOfIngredientNames[k].equals(recipeIngredient)) {
 						listOfRecipeCalories[i] += listOfCaloriesPerOunce[k] * ouncesOfIngredient; 
 						listOfRecipePrices[i] += listOfPricePerOunce[k] * ouncesOfIngredient;
 						if (listOfIsVegetarian[k] != true) {
 							listOfRecipeVegetarian[i] = false;
 						}
 					}
 				}
 			}	
 		}
		s.close();
		
		String[] vegetarianOrNot = new String[numberOfRecipes];
 		for (int i = 0; i < listOfRecipeVegetarian.length; i++) {
 			if (listOfRecipeVegetarian[i] == true) {
 				vegetarianOrNot[i] = "Vegetarian";
 			} else {
 				vegetarianOrNot[i] = "Non-Vegetarian";
 			}
 		}
 		
 		for (int i = 0; i < numberOfRecipes; i++) {
 			System.out.println(listOfRecipeNames[i] + ":");
 			System.out.println("  " +  ((int) (listOfRecipeCalories[i] + 0.5))  + " calories");
 			System.out.println("  " + "$" + String.format("%.2f", listOfRecipePrices[i]));
 			System.out.println("  " + vegetarianOrNot[i]);
 		}
	}
}
