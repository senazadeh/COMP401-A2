package a2;

import java.util.Scanner;

public class A2Jedi {

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
	  		
	 		int numberOfIngredients = s.nextInt();
	 		
	 		String[] listOfIngredientNames = new String[numberOfIngredients];
	 		double[] listOfPricePerOunce = new double[numberOfIngredients];
	 		boolean[] listOfIsVegetarian = new boolean[numberOfIngredients];
	 		int[] listOfCaloriesPerOunce = new int[numberOfIngredients];
	 		
	 		for (int i = 0; i < numberOfIngredients; i++) {
	 			String ingredientName = s.next();
	 			listOfIngredientNames[i] = ingredientName;
	 			double pricePerOunce = s.nextDouble();
	 			listOfPricePerOunce[i] = pricePerOunce;
	 			boolean isVegtarian = s.nextBoolean();
	 			listOfIsVegetarian[i] = isVegtarian;
	 			int caloriesPerOunce = s.nextInt();
	 			listOfCaloriesPerOunce[i] = caloriesPerOunce;
	 		}
	 		 		
	 		int numberOfRecipes = s.nextInt();
	 		String[] listOfRecipeNames = new String[numberOfRecipes];
	 		double[][] amounts = new double[numberOfRecipes][numberOfIngredients];
	 		
	 		for (int i = 0; i < numberOfRecipes; i++) {
	 			String recipeName = s.next();
	 			listOfRecipeNames[i] = recipeName;
	 			int numberOfIngredientsNeeded = s.nextInt();
	 			for (int j = 0; j < numberOfIngredientsNeeded; j++) {
	 				String ingredientName = s.next();
	 				double amountOfIngredient = s.nextDouble();
	 				for (int k = 0; k < numberOfIngredients; k++) {
	 					if (listOfIngredientNames[k].equals(ingredientName)) {
	 						amounts[i][k] += amountOfIngredient;
	 					}
	 				}
	 			}
	 		}
	 		
	 		double[] totalAmountOfEachIngredientNeeded = new double[numberOfIngredients];
	 		
 	 		String nameOfOrder = s.next();
	 		while (!nameOfOrder.equals("EndOrder")) {
	 	 		for (int i = 0; i < numberOfRecipes; i++) {
	 	 			if (listOfRecipeNames[i].equals(nameOfOrder)) {
	 	 				for (int j = 0; j < numberOfIngredients; j++) {
	 	 					totalAmountOfEachIngredientNeeded[j] += amounts[i][j];
	 	 				}
	 	 			}
	 	 		}
	 	 		nameOfOrder = s.next();
	 		}
			s.close();
				
			System.out.println("The order will require:");
			for(int i = 0; i < numberOfIngredients; i++) {
				System.out.println(String.format("%.2f", totalAmountOfEachIngredientNeeded[i]) + " ounces of " + listOfIngredientNames[i]);
			}
		}
	}
