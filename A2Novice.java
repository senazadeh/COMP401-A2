package a2;

import java.util.Scanner;

public class A2Novice {

	public static void main(String[] args) {
		
		Scanner s = new Scanner(System.in); 
 		
 		int numberOfIngredients = s.nextInt();
 		
 		String[] listOfIngredientNames = new String[numberOfIngredients];
 		double[] listOfPricePerOunce = new double[numberOfIngredients];
 		boolean[] listOfIsVegetarian = new boolean[numberOfIngredients];
 		int[] listOfCaloriesPerOunce = new int[numberOfIngredients];
 		
 		for (int i = 0; i < numberOfIngredients; i++) {
 			String Ingredient_Name = s.next();
 			listOfIngredientNames[i] = Ingredient_Name;
 			double Price_Per_Ounce = s.nextDouble();
 			listOfPricePerOunce[i] = Price_Per_Ounce;
 			boolean Is_Vegtarian = s.nextBoolean();
 			listOfIsVegetarian[i] = Is_Vegtarian;
 			int Calories_Per_Ounce = s.nextInt();
 			listOfCaloriesPerOunce[i] = Calories_Per_Ounce;
 		}
		s.close();
 		
 		int NumberOfVegetarianIngredients = 0;
 		for (int i = 0; i < listOfIsVegetarian.length; i++) {
 			if (listOfIsVegetarian[i] == true) {
 				NumberOfVegetarianIngredients++;
 			}
 		}
 		
 		double[] listOfCaloriesPrice = new double[numberOfIngredients];
 		for (int i = 0; i < numberOfIngredients; i++) {
 			listOfCaloriesPrice[i] = listOfCaloriesPerOunce[i] / listOfPricePerOunce[i];
 		}
 		
 		double highestCaloriePrice = 0;
 		for (int i = 0; i < listOfCaloriesPrice.length; i++) {
 			if (listOfCaloriesPrice[i] > highestCaloriePrice) {
 				highestCaloriePrice = listOfCaloriesPrice[i]; }}
 		String IngredientWithHighestRatio = "";
 		for (int i = 0; i < listOfCaloriesPrice.length; i++) {
 			if (highestCaloriePrice == listOfCaloriesPrice[i]) {
 				IngredientWithHighestRatio = listOfIngredientNames[i];
 			}
 		}
 		
 		double lowestCaloriePrice = 999999999;
 		for (int i = 0; i < listOfCaloriesPrice.length; i++) {
 			if (listOfCaloriesPrice[i] < lowestCaloriePrice) {
 				lowestCaloriePrice = listOfCaloriesPrice[i]; }}
 		String IngredientWithLowestRatio = "";
 		for (int i = 0; i < listOfCaloriesPrice.length; i++) {
 			if (lowestCaloriePrice == listOfCaloriesPrice[i]) {
 				IngredientWithLowestRatio = listOfIngredientNames[i];
 			}
 		}
 		
 		System.out.println("Number of vegetarian ingredients: " + NumberOfVegetarianIngredients);
 		System.out.println("Highest cals/$: " + IngredientWithHighestRatio);
 		System.out.println("Lowest cals/$: " + IngredientWithLowestRatio);
 		
	}	
}
